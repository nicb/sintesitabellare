# SINTESI TABELLARE

## Overview

This project leverages Csound, a software synthesis and audio programming language, to generate audio files. The core functionalities are managed by a Python script (`main.py`) and a Makefile that automates the compilation and rendering process. Additionally, the project integrates seamlessly with Reaper for dynamic audio visualization and manipulation.

## Project Structure

The project is structured into the following directories:

- `csound`: Contains Csound-related files, including `.orc`,`.sco` file, and Csound libraries.
- `python`: Includes Python scripts responsible for generating Csound files (`main.py`), manipulating metadata.
- `plotter`: Contains Reaper project files and output directories for audio files.
- `prgMake`: Contains Makefile files divided into various sections to facilitate project management.

## Dependencies

Ensure the following dependencies are installed:

- Python 3.11
- json Python library
- sys Python library
- Csound version 6.18
- Reaper 7 (for audio visualization and action time manipulation)
- GNU Make or Cmake

## Usage

The system is controlled by various Makefile targets, which can be executed from a terminal. Below are the main targets and their functionalities:

### Python:

- **run_python:** Executes the main Python script (*main.py*) and, if output produced, initiates Csound to generate audio files.

- **run_python_all_files:** Executes the main Python script for all .sco files present in the directory, generating audio files for each.

- **run_python_only:** Executes only the main Python script without initiating Csound.

### Csound:

- **run_csound:** Executes Csound on the .sco files and generates a .wav audio file.

- **run_csound_all_files:** Executes Csound for every .sco file present in the directory.

- **run_csound_choosing_files:** Executes Csound using the specified .sco, .orc, and .wav files.
For example, to use a file named `my_orchestra.orc`, modify the Makefile as follows:
```Cmake
ORC = my_orchestra.orc
```
For instance, if your desired .sco file is named custom.sco, modify the Makefile as follows:
```Cmake
SCO = custom.sco
```
For instance, to name the output file new_output.wav, modify the Makefile as follows:
```Cmake
WAV = new_output.wav
```
You can change all three parameters (ORC, SCO, and WAV) in a single line by providing values for each of them. Here's an example:
```Cmake
make run_csound_choosing_files ORC=my_orchestra.orc SCO=custom.sco WAV=new_output.wav
```

### Clean:

- **cleanPrg:** Removes all generated files, including audio files, Reaper project files, and temporary Csound files.

- **cleanAll:** Removes all generated files inside the sco and wav directories. 

- **cleanTables:** Removes all temporary Csound file tables.

- **cleanPyAll:** Removes all generated files inside the sco and tables directories. 

### Utility:

- **run:** Opens Reaper with the specified project.
- **drag:** Opens the folder containing the output audio files.
- **helpme:** Displays a list of all available targets with their descriptions.

### Alias target:

- **rp:** Alias for the `run_python` target.
- **rpaf:** Alias for the `run_python_all_files` target.
- **rpo:** Alias for the `run_python_only` target.
- **rc:** Alias for the `run_csound` target.
- **rcaf:** Alias for the `run_csound_all_files` target.
- **rccf:** Alias for the `run_csound_choosing_files` target.

### Update Metadata:

Modify the metadata files in the metadata directory to configure audio generation parameters.

## Additional Notes

- Directory Structure:

Maintain the existing directory structure to ensure the correct functioning of scripts and Makefile.

- Dependencies:

Keep dependencies updated for a smooth execution.

- License:

<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/GiulioDeMattia/sintesitabellare">Sintesi tabellare</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://gitlab.com/GiulioDeMattia">Giulio Romano De Mattia</a> is licensed under <a href="http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">Attribution 4.0 International<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"></a></p>