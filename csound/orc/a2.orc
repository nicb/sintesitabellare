sr = 192000       ; Sampling frequency
ksmps = 32        
nchnls = 2        ; Number of channels (stereo)
0dbfs = 6         ; Maximum signal level

#include "lib/msPan.lib"  ; Include the "msPan.lib" library for stereo panning

zakinit 1000,1000   ; Initialize the ZAK system with 1000 input/output channels

;--- Definition of instruments 1-99 ---
instr 1
    ifreq=p4        ; Signal frequency
    iamp1=p5        ; Amplitude of the first oscillator
    iamp2=p6        ; Amplitude of the second oscillator
    iamp3=p7        ; Amplitude of the third oscillator
    ifn1=p8         ; Table for the first oscillator
    ifn2=p9         ; Table for the second oscillator
    ifn3=p10        ; Table for the third oscillator
    idev2=p11       ; Deviation of the second oscillator
    idev3=p12       ; Deviation of the third oscillator
    ifnF = p13
    ifnA1 = p14
    ifnA2 = p15
    ifnA3 = p16
    ifndev2 = p17
    ifndev3 = p18
    ; Generate vibrato using rspline
    ;kvib	rspline	1,10,0.07,0.1
    ;kvib = kvib * .01

    ; Create a cosine envelope
    ;kenv 	cosseg	0,	p3*.7,	1, p3*.2, 1, p3*.1,	0
    kphasor line 0, p3,1
    kamp1 = iamp1 * table:k(kphasor, ifnA1, 1)
    kamp2 = iamp2 * table:k(kphasor, ifnA2, 1)
    kamp3 = iamp3 * table:k(kphasor, ifnA3, 1) 
;printk 0.1 , kamp1
    kfreq = ifreq * table:k(kphasor, ifnF, 1)

    kdev2 = idev2 * table:k(kphasor, ifndev2, 1)
    kdev3 = idev3 * table:k(kphasor, ifndev3, 1)

    ; Generate three oscillators using poscil3
    as1 poscil3 kamp1, kfreq,           ifn1
    as2 poscil3 kamp2, kfreq+kdev2,     ifn2
    as3 poscil3 kamp3, kfreq+kdev3,	    ifn3

    ; Output to the ZAK Bus
    zaw as1, p1
    zaw as2, p1+1
    zaw as3, p1+2
endin

;--- Instrument 100 --- DETACHED PARTIALS
instr 100
    ifreq = p4
    ifn1 = p5
    ifn2 = p6
    ifn3 = p7
    iamp1 = p8
    iamp2 = p9
    iamp3 = p10
    ifnA1 = p11
    ifnA2 = p12
    ifnA3 = p13

    kphasor line 0, p3,1
    kamp1 = iamp1 * table:k(kphasor, ifnA1, 1)
    kamp2 = iamp2 * table:k(kphasor, ifnA2, 1)
    kamp3 = iamp3 * table:k(kphasor, ifnA3, 1)

    kf1	rspline	1, 2, .1, .3
    aParz1	poscil3	kamp1,	ifreq+kf1, ifn1

    kf2	rspline	.2, .5, .09, .2
    aParz2	poscil3	kamp2,	ifreq+kf2, ifn2

    kf3	rspline	.01, .2, .03, .09
    kf3 = kf3 *1
    aParz3	poscil3	kamp3,	ifreq+kf3, ifn3

    zaw aParz1, p1
    zaw aParz2, p1+1
    zaw aParz3, p1+2
endin

;--- Instrument 200 ---
instr   200

    itheta=p4        ; Azimuth Angle
    iwidth=p5        ; Azimuth Width
    iparts=p6        ; Partial Presence
    ipolarpattern=p7 ; Mid Polar curve 
    ifnTheta = p8
    ifnWidth = p9
    ifnParts=p10
    ifnPolarpattern = p11

    as1  zar p1-200+1
    as2  zar p1-200+2
    as3  zar p1-200+3
    aParz1  zar p1-100
    aParz2  zar p1-100+1
    aParz3  zar p1-100+2

    kphasor line 0, p3,1
    kwid = iwidth * table:k(kphasor, ifnWidth, 1)
    ktheta = itheta * table:k(kphasor, ifnTheta, 1)
    kparts = iparts * table:k(kphasor, ifnParts, 1)
    kpolarpattern = ipolarpattern * table:k(kphasor, ifnPolarpattern, 1)

    kwidth  rspline 0,  kwid/6, 1,    3
    kwidth1 rspline 0,  kwid/4, 0.17,    0.27
    kwidth2 rspline  0,  kwid/8, 0.12,    0.33
    kwidth  = kwidth-kwid/2
    kwidth1 = kwidth-kwid/2
    kwidth2 = kwidth-kwid/2

    aParz1 = aParz1*kparts
    aParz2 = aParz2*kparts
    aParz3 = aParz3*kparts
    kdeg1 = ktheta+kwidth
    kdeg2 = ktheta+kwidth1
    kdeg3 = ktheta+kwidth1 * (random:i(1,100))*.02
    kdeg4 = ktheta+kwidth2
    kdeg5 = ktheta+kwidth2 * (random:i(1,100))*.03
    kdeg6 = ktheta+kwidth2 * (random:i(1,100))*.07

    ; Use MsPan to manipulate stereo panorama
    aM1,aS1     MsPan as1,      kdeg1,  kpolarpattern
    aM2,aS2     MsPan as2,      kdeg2,  kpolarpattern
    aM3,aS3     MsPan as3,      kdeg3,  kpolarpattern
    aM4,aS4     MsPan aParz1,   kdeg4,  kpolarpattern
    aM5,aS5     MsPan aParz2,   kdeg5,  kpolarpattern
    aM6,aS6     MsPan aParz3,   kdeg6,  kpolarpattern

    ; Combine signals and apply a high-pass filter
    aoutM = (aM1 + aM2 + aM3 + aM4 + aM5 + aM6)
    aoutS = (aS1 + aS2 + aS3 + aS4 + aS5 + aS6)
;    aout1 butterhp		aoutM, 60
;    aout2 butterhp		aoutS, 60

    ; Output MS signal
    outs aoutM, aoutS
endin
