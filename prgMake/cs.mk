# Makefile for Csound Project

# Include initialization rules
include $(realpath init.mk)
# Include utility rules
include $(realpath utility.mk)

# Target: verifying_dirs
# Description: Verify if the necessary directories exist
verifying_dirs: create_outputWav_folder
	@if [ ! -d "$(CS_DIR)/sco" ]; then \
		echo "The 'sco' directory does not exist"; \
		exit 1; \
	fi

# Target: run_csound
# Description: Execute Csound on the designated .sco file and generate a WAV file
run_csound: verifying_dirs
	@instr=$$(awk 'NR==2 {sub(/; /, "", $$0); print}' $(CS_DIR)/$(SCO_DIR)/$(SCO)); \
	echo "Extracted: $$instr"; \
	orc_file=$$instr.orc; \
	echo "Selected .orc file: $$orc_file"; \
	wav_file=$(WAV_DIR)/$(basename $(SCO)).wav; \
	echo "Ready to create $$wav_file"; \
	cd $(CS_DIR) && echo "Running Csound..." && csound --env:INCDIR+=$(LIB_ABS_PATH) --format=wav -o ../$$wav_file $(ORC_DIR)/$$orc_file $(SCO_DIR)/$(SCO)
# Alias for executing the 'run_csound' target.
rc: run_csound




# Target: run_csound_all_files
# Description: Execute Csound on all .sco files in the designated directory and generate WAV files for each
run_csound_all_files: verifying_dirs
	@for file in $$(find $(CS_DIR)/$(SCO_DIR) -name '*.sco'); do \
		instr=$$(awk 'NR==2 {sub(/; /, "", $$0); print}' $$file); \
		echo "Extracted: $$instr"; \
		orc_file=$$instr.orc; \
		echo "Selected .orc file: $$orc_file"; \
		echo "Processing file: $$file"; \
		filename=$$(basename "$$file" .sco); \
		echo "Running Csound on $$filename..."; \
		cd $(CS_DIR) && csound --env:INCDIR+=$(LIB_ABS_PATH) --format=wav -o ../$(WAV_DIR)/$$filename.wav $(ORC_DIR)/$$orc_file sco/$$filename.sco; \
		cd ..; \
	done
# Alias for executing the 'run_csound_all_sco' target.
rcaf: run_csound_all_files




# Target: run_csound_choosing_files
# Description: Execute csound choosing SCO, ORC and WAV values 
run_csound_choosing_files: verifying_dirs
	cd $(CS_DIR) && echo "Running Csound..." && csound --env:INCDIR+=$(LIB_ABS_PATH) --format=wav -o ../$(WAV_DIR)/$(WAV) $(ORC_DIR)/$(ORC) $(SCO_DIR)/$(SCO)
# Alias for executing the 'run_csound_choosing_files' target.
rccf: run_csound_choosing_files