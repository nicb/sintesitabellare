# Include initialization rules
include $(realpath init.mk)

# Target: cleanTables
# Description: Remove all files in csound/tables
cleanTables:
	rm -f $(CS_DIR)/tables/* 

cleanScos:
	rm -f $(CS_DIR)/sco/* 

cleanWav:
	rm -f $(WAV_DIR)/*.wav

cleanReapeaks:
	rm -f $(WAV_DIR)/*.reapeaks



cleanPyAll: cleanScos cleanTables

# Target: cleanAll
cleanAll: cleanScos cleanWav
	

# Target: cleanPrg
# Description: Remove all files in csound/sco, plotter/Media, and plotter/outputWav/.wav
cleanPrg: cleanTables cleanWav
	rm -f $(REAPER_PATH)/Media/*

