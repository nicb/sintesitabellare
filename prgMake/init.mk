# DIRECTORIES

# Directory containing files for Csound
CS_DIR = csound
# Directory containing the .py file for python
PY_DIR = python
# Directory containing the .wav file for Reaper
WAV_DIR = plotter/outputWav
# Directory containing the .RPP 
REAPER_PATH = plotter
# Directory containing the csound Libraries - relative path
LIB_RELATIVE_PATH = csound/lib
# Directory containing the csound Libraries - absolute path
LIB_ABS_PATH := $(realpath $(LIB_RELATIVE_PATH))
# Directory containing the .sco file for Csound
SCO_DIR = sco
# Directory containing the .orc file for Csound
ORC_DIR = orc


# FILES

# Name of the Reaper project file
REAPER_PROJECT = $(REAPER_PATH)/plotter.RPP
# Name of the Python script
PY_SCRIPT = main.py
# Name of the .orc file for Csound
ORC ?= a1.orc
# Name of the .sco file for Csound
# |----  on shell ---->  '''make SCO=items.sco'''
SCO ?= file1.sco
# Name of the .wav file generated
# |----  on shell ---->  '''make WAV=nuovo_nome.wav'''
WAV ?= test.wav
