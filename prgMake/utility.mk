# Include initialization rules
include $(realpath init.mk)

# Target: run
# Description: Open REAPER with the specified project and then invoke the 'drag' target
run:
	@echo "Opening REAPER..."
	open -a "REAPER" "$(REAPER_PROJECT)"
	make drag

# Target: drag
# Description: Open the WAV_DIR folder
drag:
	open $(WAV_DIR)

# Rule: create_outputWav_folder
# Description: Create the "outputWav" folder if it doesn't exist
create_outputWav_folder:
	@if [ ! -d "$(WAV_DIR)" ]; then \
		mkdir -p "$(WAV_DIR)"; \
		echo "Directory 'outputWav' created in $(WAV_DIR)"; \
	fi
