# This Makefile is used to automate various tasks,
# including compiling code, running tests, cleaning up files, and executing
# Python scripts.

# Include initialization rules
include $(realpath init.mk)

# Include cleanup rules
include $(realpath clean.mk)

# Include rules related to Csound
include $(realpath cs.mk)

# Target: run_python
# Description: Run the Python script and execute Csound if the script produces no output
run_python: cleanPyAll
	@output=$$(python3 $(PY_DIR)/$(PY_SCRIPT) 2>&1); \
    ret=$$?; \
    if [ $$ret -eq 0 ]; then \
        echo "Done without errors"; \
		echo "Running all Csound..."; \
		make run_csound; \
    else \
        echo "Error running Python script (exit code $$ret): $$output"; \
    fi	
# Alias for executing the 'run_python' target.
rp: run_python


# Target: run_python_all_files
# Description: Run the Python script and execute Csound for all .sco files if the script produces no output
run_python_all_files: cleanTables
	@output=$$(python3 $(PY_DIR)/$(PY_SCRIPT) 2>&1); \
    ret=$$?; \
    if [ $$ret -eq 0 ]; then \
        echo "Done without errors"; \
		echo "Running all Csound..."; \
		make run_csound_all_files; \
    else \
        echo "Error running Python script (exit code $$ret): $$output"; \
    fi
# Alias for executing the 'run_python_all_files' target.
rpaf: run_python_all_files

# Target: all_sco_only
# Description: Execute only the Python script and output the result to .sco files
run_python_only: cleanPyAll
	@output=$$(python3 $(PY_DIR)/$(PY_SCRIPT) 2>&1); \
    ret=$$?; \
    if [ $$ret -eq 0 ]; then \
        echo "Done without errors"; \
    else \
        echo "Error running Python script (exit code $$ret): $$output"; \
    fi	

# Alias for executing the 'all_sco_only' target.
rpo: run_python_only

run_python_choosing_files: rpo rccf

rpcf: run_python_choosing_files