include $(realpath prgMake/init.mk)

all: rpaf run

include $(realpath prgMake/clean.mk)
include $(realpath prgMake/utility.mk)
include $(realpath prgMake/cs.mk)
include $(realpath prgMake/py.mk)
include $(realpath prgMake/ffmpg.mk)

# Help target: Display information about available targets and their usage
helpme:
	@echo "Available targets:" 
	@echo ""
	@echo ""
	@echo ""
	@echo ""
	@echo "  all                 			: Target for executing 'run_python_all_files' and 'run' target" 
	@echo ""
	@echo ""
	@echo ""
	@echo "Python:"
	@echo "  run_python        				: Execute the Python script and run Csound if output is produced" 
	@echo "  rp                 			: Alias for executing 'run_python' target"
	@echo ""
	@echo "  run_python_all_files   		: Execute the Python script and run Csound for any .sco file if output is produced"
	@echo "  rpaf                   		: Alias for executing 'run_python_all_files' target"
	@echo ""
	@echo "  run_python_only 	    		: Execute just the Python script"
	@echo "  rpo                 			: Alias for executing 'run_python_only' target"
	@echo ""
	@echo "  run_python_choosing_files 		: Execute the Python script and run Csound choosing SCO, ORC and WAV values"
	@echo "	 rpcf							: Alias for executing 'run_python_choosing_files' target"			
	@echo ""
	@echo ""
	@echo ""
	@echo "Csound:"
	@echo "  run_csound          			: Execute Csound on the .sco files and generate a WAV file"
	@echo "  rc                				: Alias for executing 'run_csound' target"
	@echo ""
	@echo "  run_csound_all_files  			: Execute Csound for every .sco file in sco directory"
	@echo "  rcaf                 			: Alias for executing 'run_csound_all_files' target"
	@echo ""
	@echo "  run_csound_choosing_files  	: Execute csound choosing SCO, ORC and WAV values"
	@echo "  rccf                 			: Alias for executing 'run_csound_choosing_files' target"
	@echo ""
	@echo ""
	@echo ""
	@echo "Utilities:"
	@echo "  run                 			: Open REAPER with the specified project"
	@echo "  drag                			: Open WAV_DIR folder"
	@echo ""
	@echo ""
	@echo ""
	@echo "Clean:"
	@echo "  cleanTables					: Remove all files in csound/tables"
	@echo "  cleanScos						: Remove all files in csound/sco"
	@echo "  cleanWav						: Remove .wav files in plotter/outputWav"
	@echo "  cleanReapeaks					: Remove .reapeaks files in plotter/outputWav"
	@echo "  cleanPyAll            			: Target for executing 'cleanScos' and 'cleanTables'"
	@echo "  cleanAll            			: Target for executing 'cleanScos' and 'cleanWav'"
	@echo "  cleanPrg	        			: Target for executing 'cleanTables', 'cleanWav' and remove all files in plotter/Media"
	@echo ""
	@echo ""
	@echo ""
	@echo "  helpme              			: Display this help message"
	@echo ""
	@echo ""
	@echo ""
	@echo "Variables (modifiable from the command line):"
	@echo "  ORC                 	: Name of the .orc file for Csound"
	@echo "  SCO                 	: Name of the .sco file for Csound"
	@echo "  WAV                 	: Name of the .wav file generated (valid only for 'target_wav_run_csound' target)"
	@echo ""
	@echo ""
	@echo ""
	@while true; do \
		echo ""; \
		echo "Enter the name of the target to view its description (or 'q' to quit):\n\n"; \
		read target; \
		if [ "$$target" = "q" ]; then \
			break; \
		elif [ "$$target" != "" ]; then \
			make $$target --dry-run || echo "Target '$$target' does not exist."; \
		fi; \
	done

.PHONY: helpme
