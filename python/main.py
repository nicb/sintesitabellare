import os
from tables import *
from sco import createSco
def main():
    """
    Execute the main process of the script.

    Read metadata from a JSON file, create instances of Gen10,
    and write details to .sco files for Csound processing.
    """
    # Get the directory of the current script
    script_directory = os.path.dirname(os.path.abspath(__file__))

    # Define paths to metadata files
    metadata_tables_input = os.path.join(script_directory, '../metadata/tables')
    metadata_sco_input = os.path.join(script_directory, '../metadata/sco')

    # Define paths to output directories
    sco_directory_out = os.path.join(script_directory, '../csound/sco')
    table_directory_out = os.path.join(script_directory, '../csound/tables')

    # Get list of all files in metadata input
    metadata_scos = [os.path.join(metadata_sco_input, file) for file in os.listdir(metadata_sco_input) if file.endswith('.json')]
    metadata_tables = [os.path.join(metadata_tables_input, file) for file in os.listdir(metadata_tables_input) if file.endswith('.json')]

    # Iterate over all files in the tables metadata directory
#    for filename in os.listdir(tables_metadata):
  #      if filename.endswith('.json'):
 #       json_file_path = os.path.join(tables_metadata, filename)
            # Process JSON file and print the result
    #        createTables(json_file_path, table_directory_out)

    # Call functions to generate .sco files based on table metadata and Csound details
    createTables(metadata_tables, table_directory_out)
    createSco(metadata_scos, sco_directory_out)

if __name__ == "__main__":
    main()
