from genBase import GenBase
class Gen7(GenBase):
    # Class variable for the function number
    function_number = 2

    def __init__(self, n_GenType, ordinate_values, length_of_segment):
        """
        Initialize Gen_segmentGenerator instance.

        :param n_GenType: GenType parameter.
        :param ordinate_values: List of ordinate values.
        :param length_of_segment: List of length values for each segment.
        """
        super().__init__(n_GenType)  
        # No default values mentioned
        self.ordinate_values = ordinate_values
        self.length_of_segment = length_of_segment
        self.scaled_length = None
        self.function_number = Gen7.get_and_increment_function_number()
        try:
            # Verifies that the number of elements in the length_of_segment list is equal to the number of elements in the ordinate_values list minus 1
            if len(self.length_of_segment) != len(self.ordinate_values) - 1:
                raise ValueError("The number of elements in the length_of_segment list must be equal to the number of elements in the ordinate_values list minus 1.")

            # Verifies that the sum of percentages is 100
            if sum(self.length_of_segment) != 100:
                raise ValueError("The sum of percentages must be exactly 100.")

            # Verifies that all ordinate values are between -1 and 1
            if any(float(g) <= -1.0001 or float(g) >= +1.0001 for g in self.ordinate_values):
                raise ValueError("Ordinate values must be between -1 and 1.")

            # Additional Gen_segmentGenerator-specific logic if needed
            self.scaled_length = [int((p/100) * GenBase.size) for p in self.length_of_segment]
        except ValueError as ve:
            # Handles the ValueError raised in the customized logic
            print(f"Error in create method: {ve}")
            # Additional error handling or logging can be added here


    def toCsound(self):
        """
        Convert Gen_segmentGenerator data to Csound format.

        :return: Csound string representation.
        """
        max_length = max(len(self.ordinate_values), len(self.scaled_length))

        # Constructs the Csound string with the class data
        alternated_values = [val for pair in zip(self.ordinate_values[:max_length], self.scaled_length[:max_length]) for val in pair]
        alternated_values.append(str(self.ordinate_values[-1]))
        csound_string = f"f{self.function_number} {self.time} {GenBase.size} {self.n_GenType} "
        csound_string += " ".join(map(str, alternated_values))
        return csound_string + "\n"

    @classmethod
    def get_and_increment_function_number(cls):
        """
        Get the current function number and increment it by 1 for the next instance.

        :return: Current function number.
        """
        current_function_number = cls.function_number
        cls.function_number += 1
        return current_function_number
