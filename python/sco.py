import os
import json
from nota1 import Nota1
from nota100 import Nota100
from nota200 import Nota200

def createSco(json_files, sco_directory):
    """Create Csound files based on the information provided in the JSON files."""
    # Check if the directory for Csound files exists, if not, create it
    if not os.path.exists(sco_directory):
        os.makedirs(sco_directory)

    # Iterate over each JSON file
    for json_file_path in json_files:
        # Open the JSON file
        with open(json_file_path, 'r') as file:
            sco_list = json.load(file)

        # Iterate over each item in the JSON file
        for sco_data in sco_list:
            item_name = sco_data["itemName"]
            sco_file_content = f"; Csound details for {item_name}\n"
            sco_file_content += f"; {sco_data['instr']}\n"
            sco_file_content += f"#include \"tables/gen7.sco\"\n"
            sco_file_content += f"#include \"tables/gen10.sco\"\n"

            try:
                # Create a Nota1 object
                nota1 = Nota1(
                    dur=sco_data["dur"],
                    freq=sco_data["freq"],
                    amp1=sco_data["ampFirst"],
                    amp2=sco_data["ampSecond"],
                    amp3=sco_data["ampThird"],
                    ifn1=sco_data["ifnFirst"],
                    ifn2=sco_data["ifnSecond"],
                    ifn3=sco_data["ifnThird"],
                    fdev2=sco_data["freqDeviation2"],
                    fdev3=sco_data["freqDeviation3"],
                    ifnF=sco_data["ifnFreq"],
                    ifnA1=sco_data["ifnAmpFirst"],
                    ifnA2=sco_data["ifnAmpSecond"],
                    ifnA3=sco_data["ifnAmpThird"],
                    ifndev2=sco_data["ifnDevF2"],
                    ifndev3=sco_data["ifnDevF3"]
                )
                sco_file_content += nota1.toCsound()  # Add Nota1 output to sco_file_content


                nota100 = Nota100(
                    dur=sco_data["dur"],
                    freq=sco_data["freq"],
                    ifn1=sco_data.get("ifnParts1", sco_data["ifnFirst"]),
                    ifn2=sco_data.get("ifnParts2", sco_data["ifnSecond"]),
                    ifn3=sco_data.get("ifnParts3", sco_data["ifnThird"]),
                    amp1=sco_data.get("Amp1", 0),
                    amp2=sco_data.get("Amp2", 0),
                    amp3=sco_data.get("Amp3", 0),
                    ifnA1=sco_data.get("ifnAmp1", sco_data["ifnAmpFirst"]),
                    ifnA2=sco_data.get("ifnAmp2", sco_data["ifnAmpSecond"]),
                    ifnA3=sco_data.get("ifnAmp3", sco_data["ifnAmpThird"])
                )
                sco_file_content += nota100.toCsound()  # Add Nota100 output to sco_file_content

                # Create a Nota200 object
                nota200 = Nota200(
                    dur=sco_data["dur"],
                    theta=sco_data.get("theta", 0.0),
                    width=sco_data.get("width", 0.0),
                    parts=sco_data.get("partials", 0),
                    polarpattern=sco_data.get("polarpattern", 0.5),
                    ifntheta=sco_data.get("ifntheta", 0),
                    ifnwidth=sco_data.get("ifnwidth", 0),
                    ifnparts=sco_data.get("ifnpartials", 0),
                    ifnpolarpattern=sco_data.get("ifnpolarpattern", 0)
                )
                
                sco_file_content += nota200.toCsound()  # Add Nota200 output to sco_file_content

                # Write the generated Csound content to a file
                sco_file_path = os.path.join(sco_directory, f"{item_name}.sco")
                with open(sco_file_path, 'w') as sco_file:
                    sco_file.write(sco_file_content)

            # Handle KeyError and TypeError exceptions
            except KeyError as e:
                print(f"Warning: Missing key in dictionary for '{item_name}': {e}")
            except TypeError as e:
                print(f"Warning: Type error in dictionary for '{item_name}': {e}")
