"""
Copyright (c) 2024 Giulio Romano De Mattia

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from nota import Nota


class Nota1(Nota):
    nInstr = 1  # Default instrument number for Nota1

    def __init__(self, dur, freq, amp1, amp2, amp3, ifn1, ifn2, ifn3, fdev2, fdev3,ifnF,ifnA1,ifnA2,ifnA3,ifndev2,ifndev3):
        """
        Initializes a Nota1 object with the specified parameters.

        Args:
        dur (float): Duration of the note.
        freq (float): Frequency of the note.
        amp1 (float): Amplitude of the first oscillator.
        amp2 (float): Amplitude of the second oscillator.
        amp3 (float): Amplitude of the third oscillator.
        ifn1 (int): Table of the first oscillator.
        ifn2 (int): Table of the second oscillator.
        ifn3 (int): Table of the third oscillator.
        fdev2 (float): Deviation of the second oscillator.
        fdev3 (float): Deviation of the third oscillator.
        """
        super().__init__(dur)
        self.freq = freq  # Frequency of the note
        self.amp1 = amp1  # Amplitude of the first oscillator
        self.amp2 = amp2  # Amplitude of the second oscillator
        self.amp3 = amp3  # Amplitude of the third oscillator
        self.ifn1 = ifn1  # Table of the first oscillator
        self.ifn2 = ifn2  # Table of the second oscillator
        self.ifn3 = ifn3  # Table of the third oscillator
        self.fdev2 = fdev2  # Deviation of the second oscillator
        self.fdev3 = fdev3  # Deviation of the third oscillator
        self.ifnF  = ifnF
        self.ifnA1  = ifnA1
        self.ifnA2  = ifnA2
        self.ifnA3  = ifnA3
        self.ifndev2  = ifndev2
        self.ifndev3  = ifndev3

    def toCsound(self):
        """
        Returns a Csound score string representation of the Nota1 object.

        Returns:
        str: Csound score string for the Nota1 object.
        """
        return f"i {Nota1.nInstr}   {self.at}  {self.dur}  {self.freq}  {self.amp1:.4f}  {self.amp2:.4f}  {self.amp3:.4f}  {self.ifn1}  {self.ifn2}  {self.ifn3}  {self.fdev2}  {self.fdev3} {self.ifnF}  {self.ifnA1}  {self.ifnA2} {self.ifnA3}  {self.ifndev2} {self.ifndev3} \n"
