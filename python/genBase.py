class GenBase:
    # Class variable for the function number
    # Base scale factor for calculations
    size = 2**14

    def __init__(self, n_GenType):
        """
        Initialize GenBase instance.

        :param time: Time parameter.
        :param size: Size parameter.
        :param n_GenType: Number of GenType parameter.
        """
        self.time = 0
        self.n_GenType = n_GenType
        # Assign the current function number to this instance

