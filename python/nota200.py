"""
Copyright (c) 2024 Giulio Romano De Mattia

MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from nota import Nota

class Nota200(Nota):
    nInstr = 200  # Default instrument number for Nota200

    def __init__(self, dur, theta, width, parts,polarpattern,ifntheta, ifnwidth, ifnparts,ifnpolarpattern):
        """
        Initializes a Nota200 object with the specified parameters.

        Args:
        dur (float): Duration of the note.
        theta (float): Theta parameter.
        width (float): Width parameter.
        parts (int): Partial Presence.
        """
        super().__init__(dur)
        self.theta = theta  # Theta parameter
        self.width = width  # Width parameter
        self.parts = parts  # Partial Presence
        self.polarpattern = polarpattern  # Polar pattern
        self.ifntheta = ifntheta  # Theta parameter
        self.ifnwidth = ifnwidth  # Width parameter
        self.ifnparts = ifnparts  # Partial Presence
        self.ifnpolarpattern = ifnpolarpattern  # Polar pattern

    def toCsound(self):
        """
        Returns a Csound score string representation of the Nota200 object.

        Returns:
        str: Csound score string for the Nota200 object.
        """
        return f"i {Nota200.nInstr}   {self.at}  {self.dur}  {self.theta}  {self.width} {self.parts} {self.polarpattern} {self.ifntheta}  {self.ifnwidth} {self.ifnparts} {self.ifnpolarpattern}\n"
