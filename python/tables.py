from gen10 import Gen10
from gen7 import Gen7
from mathFunc import fibonacciList, primerangeList
import os
import sys
import json



def generate_sequence(choice, startnum, length_list):
    try:
        if choice == "fib":
            return fibonacciList(startnum, length_list)
        elif choice == "prime":
            return primerangeList(startnum, length_list)
        else:
            raise ValueError("Invalid sequence type")
    except Exception as e:
        # Handle any exceptions that might occur during the process
        # Print the error to stderr
        sys.stderr.write(f"Error: {type(e).__name__} - {e}\n")
        sys.exit(1)  # Exit with a non-zero exit code to indicate an error





def write_GenTables(tables_directory, Gen,json_filename):
    """
    Write Csound details for the Gen instance to a .sco file.

    Parameters:
    - tables_directory (str): The directory where the .sco file will be generated.
    - Gen (Gen10): An instance of Gen10.
    """
    # Generate the absolute path for the .sco file based on the iteration index
    gen_file_content = os.path.join(tables_directory, f"{json_filename}.sco")

    # Open the .sco file and append the Csound details for the Gen instance
    with open(gen_file_content, 'a') as sco_file:
        sco_file.write(Gen.toCsound())





def createTables(json_files, tables_directory):
    """
    Processa un file JSON.

    Parameters:
    - json_file_path (str): Il percorso del file JSON da elaborare.
    - tables_directory (str): La directory in cui verranno scritti i file .sco.
    """
    if not os.path.exists(tables_directory):
        os.makedirs(tables_directory)

    # Iterate over each JSON file
    for json_file_path in json_files:
        # Open the JSON file

        with open(json_file_path, 'r') as file:
            tables_list = json.load(file)

        try:
            # Estrae il nome del file (senza estensione)
            json_filename = os.path.splitext(os.path.basename(json_file_path))[0]

            # Verifica se esiste una funzione con lo stesso nome del file JSON nel modulo corrente
            if hasattr(sys.modules[__name__], json_filename):
                # Se esiste, chiama la funzione
                getattr(sys.modules[__name__], json_filename)(tables_list, tables_directory,json_filename)
            else:
                print(f"Warning: No function found for JSON file '{json_filename}'. Skipping...")

        except Exception as e:
            # Gestisce eventuali eccezioni durante il processo
            sys.stderr.write(f"Error: {type(e).__name__} - {e}\n")
            sys.exit(1)


def gen7(tables_list, tables_directory,json_filename):
    try:
        # Iterate over table metadata and create Gen instances
        for idx, table_data in enumerate(tables_list, start=1):
            # Extract necessary parameters from the dictionary
            n_GEN = table_data.get('n_GenType')
            if n_GEN == 7:
                gen_instance = Gen7(n_GEN, table_data.get('ordinate_values'),table_data.get('length_of_segment'))
                write_GenTables(tables_directory, gen_instance,json_filename)
            elif n_GEN == 0:
                continue
            else:
                # Raise an exception for an unknown n_GEN value
                raise ValueError(f"Unsupported n_GEN value: {n_GEN} at table: {idx}")

    except Exception as e:
        # Handle any exceptions that might occur during the process
        # Print the error to stderr
        sys.stderr.write(f"Error: {type(e).__name__} - {e}\n")
        sys.exit(1)  # Exit with a non-zero exit code to indicate an error


def gen10(tables_list, tables_directory,json_filename):
    try:
        # Iterate over table metadata and create Gen instances
        for idx, table_data in enumerate(tables_list, start=1):
            # Extract necessary parameters from the dictionary
            n_GEN = table_data.get('n_GenType')

            if n_GEN == 10:
                # Generate an instance of Gen10
                try:
                    partials = list(map(int, table_data.get('partials')))
                    if all(x >= 1 for x in partials):
                        # Generate a list of prime numbers within the specified range
                        partials_1 = generate_sequence(table_data.get("type", "prime"),partials[0], partials[1])
                        gen_instance = Gen10(n_GEN, partials_1,table_data.get("type", "prime"))
                        write_GenTables(tables_directory, gen_instance,json_filename)
                    else:
                        raise ValueError(
                            f"Invalid partials values for n_GEN {n_GEN}: {partials}. All partials must be integers equal or larger than 1.")
                except ValueError:
                    raise ValueError(
                        f"Invalid partials values for n_GEN {n_GEN}: {partials}. All partials must be integers equal or larger than 1.")
            elif n_GEN == 0:
                continue
            else:
                # Raise an exception for an unknown n_GEN value
                raise ValueError(f"Unsupported n_GEN value: {n_GEN} at table: {idx}")

    except Exception as e:
        # Handle any exceptions that might occur during the process
        # Print the error to stderr
        sys.stderr.write(f"Error: {type(e).__name__} - {e}\n")
        sys.exit(1)  # Exit with a non-zero exit code to indicate an error


